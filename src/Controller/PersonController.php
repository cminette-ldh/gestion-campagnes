<?php

namespace App\Controller;

use App\Entity\ComiteRegional;
use App\Entity\Federation;
use App\Entity\Person;
use App\Entity\Section;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ImportType;
use PhpOffice\PhpSpreadsheet;

class PersonController extends AbstractController
{
    /**
     * @Route("/person", name="person_home")
     */
    public function index()
    {
        return $this->redirectToRoute("person_list");
    }

    /**
     * @Route("/person/list", name="person_list")
     */
    public function list()
    {
        $persons = $this->getDoctrine()
            ->getRepository(Person::class)
            ->findAll();

        if (count($persons) == 0) {
            return $this->redirectToRoute("person_import");
        }

        return $this->render(
            'person/list.html.twig',
            [
                'type' => "personnes",
                'persons' => $persons,
                'count' => count($persons),
            ]
        );
    }

    /**
     * @Route("/person/import", name="person_import")
     */
    public function import(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(ImportType::class, ['libelle' => 'Membres des bureaux']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             *
             *
             * @var UploadedFile $file
             */
            $uploadedFile = $form->get('file')->getData();

            if ($uploadedFile) {
                try {
                    $spreadsheet = PhpSpreadsheet\IOFactory::load($uploadedFile);
                    $rows = $spreadsheet->getActiveSheet()->toArray();

                    unset($rows[0]);

                    foreach ($rows as $row) {
                        $person = $this->getDoctrine()
                            ->getRepository(Person::class)
                            ->findOneBy(['email' => "$row[8]"]);

                        // Region
                        $region = $this->getDoctrine()
                            ->getRepository(ComiteRegional::class)
                            ->findOneBy(['libelle' => "$row[1]"]);

                        // Federation
                        $federation = $this->getDoctrine()
                            ->getRepository(Federation::class)
                            ->findOneBy(['libelle' => "$row[2]"]);

                        if ($federation!=null && $region != null) {
                            $region->addFederation($federation);
                            $entityManager->persist($region);
                        }

                        // Section
                        $section = $this->getDoctrine()
                            ->getRepository(Section::class)
                            ->findOneBy(['libelle' => "$row[3]"]);

                        if ($section!=null && $federation != null) {
                            $federation->addSection($section);
                            $entityManager->persist($federation);
                        }

                        // Person
                        if ($person == null) {
                            $person = new Person();
                        }

                        $person->setTitle($row[5]);
                        $person->setFirstName($row[7]);
                        $person->setLastName($row[6]);
                        $person->setEmail($row[8]);
                        $person->setPhoneHome($row[9]);

                        $entityManager->persist($person);
                        $entityManager->flush();

                        if ($row[1] == $row[3]) {
                            $type = "Comité Régional";
                        } elseif ($row[2] == $row[3]) {
                            $type = "Fédération";
                        } else {
                            $type = "Section";
                        }

                        switch ($type) {
                            case "Comité Régional":
                                $repository =  $this->getDoctrine()->getRepository(ComiteRegional::class);
                                break;
                            case "Fédération":
                                $repository =  $this->getDoctrine()->getRepository(Federation::class);
                                break;
                            case "Section":
                            default:
                                $repository =  $this->getDoctrine()->getRepository(Section::class);
                                break;
                        }

                        $structure = $repository->findOneBy(['libelle' => "$row[3]"]);

                        if ($structure != null) {
                            switch (trim($row[4])) {
                                case "a pour Trésorier":
                                    $structure->setTreasurer($person);
                                    break;
                                case "a pour Secrétaire":
                                    $structure->setSecretary($person);
                                    break;
                                case "a pour président":
                                    $structure->setPresident($person);
                                    break;
                            }
                            $entityManager->persist($person);
                        }
                    }

                    $entityManager->flush();

                    return $this->redirectToRoute("person_list");
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
        }

        return $this->render(
            'generic/import.html.twig',
            [
                'items' => "personnes",
                'form' => $form->createView(),
            ]
        );
    }
}

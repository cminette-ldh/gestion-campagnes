<?php

namespace App\Controller;

use App\Entity\Section;
use App\Entity\Federation;
use App\Entity\ComiteRegional;
use App\Form\ImportType;
use PhpOffice\PhpSpreadsheet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StructureController extends AbstractController
{
    /**
     * @Route("/structures", name="structure_home")
     * @return Response
     */
    public function index(): Response
    {
        return $this->redirectToRoute("sections_list");
    }

    /**
     * @Route("/sections/list/details", name="sections_list_details")
     * @return Response
     */
    public function listDetails(): Response
    {
        $sections = $this->getDoctrine()
            ->getRepository(Section::class)
            ->findAll();

        if (count($sections)==0) {
            return $this->redirectToRoute("structures_import");
        }

        $sections = $this->getDoctrine()
            ->getRepository(Section::class)
            ->findAll();

        return $this->render(
            'structure/details.html.twig',
            [
                'type' => "sections",
                'structures' => $sections,
                'count' => count($sections),
            ]
        );
    }

    /**
     * @Route("/sections/list/", name="sections_list")
     * @return Response
     */
    public function list(): Response
    {
        $structures = $this->getDoctrine()
            ->getRepository(Section::class)
            ->findAll();

        if (count($structures)==0) {
            return $this->redirectToRoute("structures_import");
        }

        $structures = $this->getDoctrine()
            ->getRepository(Section::class)
            ->findAll();

        return $this->render(
            'structure/list.html.twig',
            [
                'type' => "sections",
                'structures' => $structures,
                'count' => count($structures),
            ]
        );
    }

    /**
     * @Route("/structures/import", name="structures_import")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function import(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(ImportType::class, ['libelle' => 'Liste des structures']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var UploadedFile $file
             */
            $uploadedFile = $form->get('file')->getData();

            if ($uploadedFile) {
                try {
                    $spreadsheet = PhpSpreadsheet\IOFactory::load($uploadedFile);
                    $rows = $spreadsheet->getActiveSheet()->toArray();

                    unset($rows[0]);

                    foreach ($rows as $row) {
                        switch ($row[0]) {
                            case "Comité Régional":
                                $structure = $this->getDoctrine()
                                    ->getRepository(ComiteRegional::class)
                                    ->findBy(["noClient" => "=$row[1]"]);

                                if ($structure == null) {
                                    $structure = new ComiteRegional();
                                }
                                break;
                            case "Fédération":
                                $structure = $this->getDoctrine()
                                    ->getRepository(Federation::class)
                                    ->findBy(["noClient" => "=$row[1]"]);

                                if ($structure == null) {
                                    $structure = new Federation();
                                }
                                break;
                            case "Section":
                            default:
                                $structure = $this->getDoctrine()
                                    ->getRepository(Section::class)
                                    ->findBy(["noClient" => "=$row[1]"]);

                                if ($structure == null) {
                                    $structure = new Section();
                                }
                                break;
                        }

                        $structure->setNoClient($row[1]);
                        $structure->setCompte($row[2]);
                        $structure->SetLibelle($row[3]);
                        $structure->SetPreferedEmailaddress($row[4]);
                        $structure->SetAddress($row[5]);
                        $structure->SetZipCode($row[6]);
                        $structure->SetTown($row[7]);
                        $structure->SetType($row[0]);

                        $entityManager->persist($structure);
                    }

                    $entityManager->flush();

                    return $this->redirectToRoute("sections_list");
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            }
        }

        return $this->render(
            'generic/import.html.twig',
            [
                'items' => "structures (sections / fédérations / comités régionaux ... ",
                'form' => $form->createView(),
            ]
        );
    }
}

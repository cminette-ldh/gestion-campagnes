<?php

namespace App\Entity;

use App\Repository\StructureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
  * @ORM\MappedSuperclass
  */
class Structure
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=7)
     */
    protected $noClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $compte;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $libelle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $preferedEmailaddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $town;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="president")
     */
    protected $president;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="treasurer")
     */
    protected $treasurer;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="secretary")
     */
    protected $secretary;


    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNoClient(): ?string
    {
        return $this->noClient;
    }

    public function setNoClient(string $noClient): self
    {
        $this->noClient = $noClient;

        return $this;
    }

    public function getCompte(): ?string
    {
        return $this->compte;
    }

    public function setCompte(?string $compte): self
    {
        $this->compte = $compte;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPreferedEmailaddress(): ?string
    {
        return $this->preferedEmailaddress;
    }

    public function setPreferedEmailaddress(?string $preferedEmailaddress): self
    {
        $this->preferedEmailaddress = $preferedEmailaddress;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPresident(): ?Person
    {
        return $this->president;
    }

    public function setPresident(?Person $president): self
    {
        $this->president = $president;

        return $this;
    }

    public function getTreasurer(): ?Person
    {
        return $this->treasurer;
    }

    public function setTreasurer(?Person $treasurer): self
    {
        $this->treasurer = $treasurer;

        return $this;
    }

    public function getSecretary(): ?Person
    {
        return $this->secretary;
    }

    public function setSecretary(?Person $secretary): self
    {
        $this->secretary = $secretary;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\FederationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FederationRepository::class)
 */
class Federation extends Structure
{
    /**
     * @ORM\OneToMany(targetEntity=Section::class, mappedBy="federation")
     */
    private $sections;

    /**
     * @ORM\ManyToOne(targetEntity=ComiteRegional::class, inversedBy="sections")
     */
    private $comiteRegional;

    public function __construct()
    {
        $this->sections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Section[]
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    public function addSection(Section $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setFederation($this);
        }

        return $this;
    }

    public function removeSection(Section $section): self
    {
        if ($this->sections->removeElement($section)) {
            // set the owning side to null (unless already changed)
            if ($section->getFederation() === $this) {
                $section->setFederation(null);
            }
        }

        return $this;
    }

    public function getComiteRegional(): ?ComiteRegional
    {
        return $this->comiteRegional;
    }

    public function setComiteRegional(?ComiteRegional $comiteRegional): self
    {
        $this->comiteRegional = $comiteRegional;

        return $this;
    }
}

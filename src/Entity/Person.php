<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneHome;

    /**
     * @ORM\OneToMany(targetEntity=Structure::class, mappedBy="president")
     */
    private $president;

    /**
     * @ORM\OneToMany(targetEntity=Structure::class, mappedBy="treasurer")
     */
    private $treasurer;

    /**
     * @ORM\OneToMany(targetEntity=Structure::class, mappedBy="secretary")
     */
    private $secretary;

    public function __construct()
    {
        $this->president = new ArrayCollection();
        $this->treasurer = new ArrayCollection();
        $this->secretary = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneHome(): ?string
    {
        return $this->phoneHome;
    }

    public function setPhoneHome(?string $phoneHome): self
    {
        $this->phoneHome = $phoneHome;

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getPresident(): Collection
    {
        return $this->president;
    }

    public function addPresident(Structure $president): self
    {
        if (!$this->president->contains($president)) {
            $this->president[] = $president;
            $president->setPresident($this);
        }

        return $this;
    }

    public function removePresident(Structure $president): self
    {
        if ($this->president->removeElement($president)) {
            // set the owning side to null (unless already changed)
            if ($president->getPresident() === $this) {
                $president->setPresident(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getTreasurer(): Collection
    {
        return $this->treasurer;
    }

    public function addTreasurer(Structure $treasurer): self
    {
        if (!$this->treasurer->contains($treasurer)) {
            $this->treasurer[] = $treasurer;
            $treasurer->setTreasurer($this);
        }

        return $this;
    }

    public function removeTreasurer(Structure $treasurer): self
    {
        if ($this->treasurer->removeElement($treasurer)) {
            // set the owning side to null (unless already changed)
            if ($treasurer->getTreasurer() === $this) {
                $treasurer->setTreasurer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getSecretary(): Collection
    {
        return $this->secretary;
    }

    public function addSecretary(Structure $secretary): self
    {
        if (!$this->secretary->contains($secretary)) {
            $this->secretary[] = $secretary;
            $secretary->setSecretary($this);
        }

        return $this;
    }

    public function removeSecretary(Structure $secretary): self
    {
        if ($this->secretary->removeElement($secretary)) {
            // set the owning side to null (unless already changed)
            if ($secretary->getSecretary() === $this) {
                $secretary->setSecretary(null);
            }
        }

        return $this;
    }
}

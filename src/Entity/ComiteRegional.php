<?php

namespace App\Entity;

use App\Repository\ComiteRegionalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComiteRegionalRepository::class)
 */
class ComiteRegional extends Structure
{
    /**
     * @ORM\OneToMany(targetEntity=Federation::class, mappedBy="comiteRegional")
     */
    private $federations;

    public function __construct()
    {
        $this->federations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Federation[]
     */
    public function getFederations(): Collection
    {
        return $this->federations;
    }

    public function addFederation(Federation $federation): self
    {
        if (!$this->federations->contains($federation)) {
            $this->federations[] = $federation;
            $federation->setComiteRegional($this);
        }

        return $this;
    }

    public function removeFederation(Federation $federation): self
    {
        if ($this->federations->removeElement($federation)) {
            // set the owning side to null (unless already changed)
            if ($federation->getComiteRegional() === $this) {
                $federation->setComiteRegional(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Repository;

use App\Entity\ComiteRegional;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ComiteRegional|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComiteRegional|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComiteRegional[]    findAll()
 * @method ComiteRegional[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComiteRegionalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComiteRegional::class);
    }

    // /**
    //  * @return ComiteRegional[] Returns an array of ComiteRegional objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ComiteRegional
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
